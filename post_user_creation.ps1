### Install Scoop ###
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
Invoke-RestMethod -Uri https://get.scoop.sh | Invoke-Expression

### Install Applications ###
scoop bucket add main
scoop bucket add extras
scoop install extras/spotify
scoop install spicetify-cli