### Install Choco ###
# https://chocolatey.org/install
Set-ExecutionPolicy Bypass -Scope Process -Force
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

### Install Applications ###
choco install -y `
    7zip winrar `               # Compression / Archiving
    gpu-z rufus `               # Hardware Utils
    virtualbox `                # Virtualization
    vscode neovim `             # Editors / IDEs
    visualstudio2022community `
    openjdk `                   # Compilers / Interpreters
    git `                       # Dev Tools
    vlc tribler `               # Media Apps
    chromium firefox `          # Browsers
    signal discord telegram `   # Messengers
    faceit                      # Gaming


winget install --id DEVCOM.JetBrainsMonoNerdFont

### Install WSL 2 ###
wsl --install --no-distribution
Invoke-WebRequest -Uri https://github.com/nix-community/NixOS-WSL/releases/download/2311.5.3/nixos-wsl.tar.gz -OutFile $env:USERPROFILE\nixos-wsl.tar.gz -UseBasicParsing
wsl --import NixOS $env:USERPROFILE\NixOS $env:USERPROFILE\nixos-wsl.tar.gz
wsl -s NixOS
# https://nix-community.github.io/NixOS-WSL/options.html

# https://learn.microsoft.com/en-us/windows/wsl/install-manual#step-1---enable-the-windows-subsystem-for-linux
#dism.exe `
#    /online `
#    /enable-feature `
#    /featurename:Microsoft-Windows-Subsystem-Linux `
#    /all `
#    /norestart
#
#dism.exe `
#    /online `
#    /enable-feature `
#    /featurename:VirtualMachinePlatform `
#    /all `
#    /norestart
#
#Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux

# https://learn.microsoft.com/en-us/windows/wsl/install
#wsl.exe --install --web-download
#wsl.exe --update --web-download
#wsl.exe --install -d Ubuntu --web-download
# reboot if this doesn't work

