# To make autounattend.xml, follow:
# https://www.tenforums.com/tutorials/96683-create-media-automated-unattended-install-windows-10-a.html#Step4
# https://www.windowscentral.com/how-create-unattended-media-do-automated-installation-windows-10#uefi_partition_setup

# For Generic Product codes:
# https://www.elevenforum.com/t/generic-product-keys-to-install-or-upgrade-windows-11-editions.3713
# https://winaero.com/windows-11-generic-keys-for-all-editions

# For Pro N: 2B87N-8KFHP-DKV6R-Y2C8J-PKCKT

# To run scripts:
# https://casits.artsandsciences.fsu.edu/how-run-powershell-scripts-windows-11

### User Creation ###
# https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.localaccounts/new-localuser?view=powershell-5.1
Write-Output "Input Admin password: "
$adminPassword = Read-Host -AsSecureString
Write-Output "Input Normal User name: "
$normalName = Read-Host
Write-Output "Input Normal User password: "
$normalPassword = Read-Host -AsSecureString

foreach ($userParams in @(
    @{
        Name        = 'admin'
        Password    = $adminPassword
        Description = 'Clean and Local Administrative User without MS Account.'
    }
    @{
        Name        = $normalName
        Password    = $normalPassword
        Description = 'Clean and Local Normal User without MS Account.'
    }
)) {
    New-LocalUser $userParams -AccountNeverExpires
}

### User Instructions ###
Write-Output "Please log-out and log-in as Admin"